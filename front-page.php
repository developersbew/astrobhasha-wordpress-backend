<?php get_header(); ?>

<div class="container">
	
	<div class="wrapper">
		
		<div class="homepage-top">
			<div class="homepage-top-heading">
				<h4>FIND OUT HOW THE STARS AND PLANETS MOVEMENT</h4>
			</div>
			<div class="homepage-top-description">
				<h3>EFFECTS YOUR LIFE</h3>
				<h3>EVERYDAY</h3>
			</div>
			<div class="homepage-top-button">
				
				<button class="button">Read More</button>
			</div>
		</div>




		<div class="astrology-category">
			<div class="astrology-category-heading">
				<h4>Look for an attitude with gratitude towards Astrology</h4>
			</div>
			<div class="row">
				<div class="col col-sm-6">
					<div class="homepage-category-image">
						<img src="<?php bloginfo('template_directory'); ?>/image/a.jpg">
					</div>
					<div class="homepage-category-title">
						<h4>2018 Horoscope</h4>
					</div>
					<div class="homepage-category-description">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco..</p>
					</div>
				</div>
				<div class="col col-sm-6">
					<div class="homepage-category-image">
						<img src="<?php bloginfo('template_directory'); ?>/image/a.jpg">
					</div>
					<div class="homepage-category-title">
						<h4>2018 Horoscope</h4>
					</div>
					<div class="homepage-category-description">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco..</p>
					</div>
				</div>
			</div>


			<div class="row">
				<div class="col col-sm-6">
					<div class="homepage-category-image">
						<img src="<?php bloginfo('template_directory'); ?>/image/a.jpg">
					</div>
					<div class="homepage-category-title">
						<h4>2018 Horoscope</h4>
					</div>
					<div class="homepage-category-description">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco..</p>
					</div>
				</div>
				<div class="col col-sm-6">
					<div class="homepage-category-image">
						<img src="<?php bloginfo('template_directory'); ?>/image/a.jpg" >
					</div>
					<div class="homepage-category-title">
						<h4>2018 Horoscope</h4>
					</div>
					<div class="homepage-category-description">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco..</p>
					</div>
				</div>
			</div>
		</div>
<!--  -->

		<div class="astrology-working">
			<div class="astrolog-image">
				1
			</div>
			<div class="astrology-working-heading">
				<h4>How Astrology Works For You</h4>
			</div>
			<div class="astrology-working-description">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
			<div class="astrology-working-consult">
				<button class="button">Consult Me</button>
				<button class="button">Read More</button>
			</div>
		</div>
<!--  -->
				

				<div class="astrobhasha-statistic">
					
					<div class="astrobhasha-statistic-heading">
						<h4>Welcome To Astrobhasha !</h4>
					</div>

					<div class="astrobhasha-statistic-description">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div>

					<div class="astrobhasha-statistic-speciality">
						<h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua.</h3>
					</div>

					<div class="astrobhasha-statistic-show">
						<div class="row">
							<div class="col">
								<div class="statistic-number"><h3>17</h3></div>
								<div class="statistic-description"><h3>Years Of experiance</h3></div>
							</div>
							<div class="col">
								<div class="statistic-number"><h3>17</h3></div>
								<div class="statistic-description"><h3>Years Of experiance</h3></div>
							</div>
							<div class="col">
								<div class="statistic-number"><h3>17</h3></div>
								<div class="statistic-description"><h3>Years Of experiance</h3></div>
							</div>
							<div class="col">
								<div class="statistic-number"><h3>17</h3></div>
								<div class="statistic-description"><h3>Years Of experiance</h3></div>
							</div>
						</div>
					</div>

					<div class="astrobhasha-statistic-subscribe">
						<h4>Subscribe Our Newsletter</h4>
						<input type="email" name="email"> <button class="button">Subscribe</button>
					</div>

				</div>

<!--  -->
					<div class="blog-videos">
						<div class="blog-videos-title">
							<h4>Blog & Videos</h4>
						</div>	

						<div class="youtube-video">
							<?php 
								$vars=array(1);
								foreach ($vars as $var):
								?>
								<div class="video-main">
									<?php echo do_shortcode("[youtube]"); ?>
								<?php endforeach;  ?>
								</div>

					</div>
						
	


<!--  -->

		</div>



	</div>
