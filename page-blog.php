<?php get_header(); ?>
<div class="blog-info">
<div class="heading">
	<h1>Blog</h1>
</div>
<div class="blog-description">
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div>
</div>

		
	<div class="blog-main">

		<div class="single-blog-category-title">

			<?php include_once('BlogWithCategory.php'); ?>

		</div>

	<?php
	echo paginate_links( array(
	    'format' => '?paged=%#%',
	    'current' => max( 1, $paged),
	    'total' => $args->max_num_pages,
	) );
	  ?>


<!-- social icons POST Share -->
<?php echo do_shortcode("[apss_share]"); ?>
<!-- social icons POST Share -->





	  	<div class="blog-slidebar">
	  		<?php  include_once('getcategory.php');?>

	
			

		<?php 
		$arg=array(
									'paged'=>$paged,
									'category_name'     =>  $category->name,
					
							        'posts_per_page'    =>  2,
									);

								$args = new WP_Query($arg);
									if($args->have_posts()):

										while($args->have_posts()): $args->the_post();?>
												
				
					
					<?php if ( $args->has_post_thumbnail() ) {	?>

<div class="blog-image" >
	<?php the_post_thumbnail(); ?>
</div>
					<?php } else { ?>
					<img src="<?php bloginfo('template_directory'); ?>/image/a.jpg" style="height: 200px;width: 300px"/>
					<?php } ?>
				<?php endwhile;?>
				</div>		
					<?php endif;?>
			</div>

					












<?php get_footer(); ?>


<!-- <ul>

<?php $the_query = new WP_Query( 'posts_per_page=2' ); ?>
 

<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
 

<a href="<?php the_permalink() ?>"><?php the_title(sprintf('<h3 class="entry-title"><a href="%s" rel="bookmark">',  esc_url(get_permalink())), '</a></h3>'); ?></a>
 

<?php echo excerpt(10); ?>

<div class="blog-sample-images">
							<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail();
							} else { ?>
							<img src="<?php bloginfo('template_directory'); ?>/image/a.jpg" height="250px" width="376"/>
							<?php } ?>
						</div>

<?php 
endwhile;
wp_reset_postdata();
?>
</ul>
 -->