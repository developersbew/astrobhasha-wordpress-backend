

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;?>

	
		<div class="entry-meta">
			<small><?php the_time('F jS, Y') ?></small>
		</div><!-- .entry-meta -->
	
	</header><!-- .entry-header -->

						<div class="blog-sample-images">
							<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail();
							} else { ?>
							<img src="<?php bloginfo('template_directory'); ?>/image/a.jpg" height="250px" width="376"/>
							<?php } ?>
						</div>

	<div class="entry-content">
		<div class="blog-desc">
							<?php echo excerpt(30);?>
							<a href="<?php the_permalink();?>">Read More</a>
						</div>
	</div><!-- .entry-content -->

