<?php
	$categories = get_categories( array(
	    'orderby' => 'name',
	    'order'=>'ASC'
	) );
	 
	foreach( $categories as $category ):
	    $category_link = sprintf( 
	    		'<a href="%1$s" alt="%2$s">%3$s</a>',
	        esc_url( get_category_link( $category->term_id ) ),
	        esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
	        esc_html( $category->name )
	    );
	    ?>
<div class="category-title" style="border: solid 1px; display: inline-table;">
	      <?php echo '<h3>'.'' . sprintf( esc_html__( '%s', 'textdomain' ), $category_link ) . ''.'</h3>'; ?>
	      </div>
	     
	    

		
		<?php 
	    
		global $paged;
		$arg=array(
			'paged'=>$paged,
			'category_name'     =>  $category->name,
	        'posts_per_page'    =>  2,
			);

		$args = new WP_Query($arg);
			if($args->have_posts()):

				while($args->have_posts()): $args->the_post();?>
						<div class="blog-title">
								<?php the_title(sprintf('<h3 class="entry-title"><a href="%s" rel="bookmark">',  esc_url(get_permalink())), '</a></h3>'); ?>
						</div>
					
						<?php if (!is_front_page()): ?>
							<div class="blog-date">
							<span class="entry-date"><?php echo get_the_date(); ?></span>
						</div>
						<?php endif ?>
					
						<div class="blog-sample-images">
							<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail();
							} else { ?>
							<img src="<?php bloginfo('template_directory'); ?>/image/a.jpg" height="250px" width="376"/>
							<?php } ?>
						</div>

				<?php if (!is_front_page()): ?>
					<?php get_template_part( 'content',get_post_format()); ?>
						<div class="blog-desc">
							<?php echo excerpt(30);?>
							<a href="<?php the_permalink();?>">Read More</a>
						</div>
				<?php endif ?>
						<?php endwhile; endif; endforeach; ?>