<?php get_header(); ?>

<div class="container">
	<div class="wrapper">
		<div class="contact-backgroud-image">
			image
		</div>

		<div class="contact-text">
			<div class="contact-heading">
				<h2>Contact Us</h2>
			</div>
			<div class="contact-description">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
		</div>

	</div>
</div>

<div class="contact-form-info">
	
	<div class="contact-from">
	<?php echo do_shortcode("[contact-form-7 id=57 title=Form]"); ?>
	</div>

	<div class="contact-info">
		<h2>Contact information</h2>
		<div class="row">
			<div class="col">
				1
			</div>
			<div class="col">
				2
			</div>
		</div>
	</div>


</div>

<?php get_footer(); ?>

