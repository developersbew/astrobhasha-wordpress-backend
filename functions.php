<?php
/**
 * astrobhasha functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package astrobhasha
 */

if ( ! function_exists( 'astrobhasha_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function astrobhasha_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on astrobhasha, use a find and replace
		 * to change 'astrobhasha' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'astrobhasha', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'astrobhasha' ),
		) );

		register_nav_menus( array(
			'menu-2' => esc_html__( 'Secondry', 'astrobhasha' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'astrobhasha_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'astrobhasha_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function astrobhasha_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'astrobhasha_content_width', 640 );
}
add_action( 'after_setup_theme', 'astrobhasha_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function astrobhasha_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'astrobhasha' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'astrobhasha' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'astrobhasha_widgets_init' );

/**
 * Enqueue scripts and styles.
 */


function astrobhasha_scripts() {

	wp_enqueue_style( 'astrobhasha-style', get_template_directory_uri().'/css/style.css');
	wp_enqueue_style( 'astrobhasha_fonts', 'https://fonts.googleapis.com/css?family=Bitter:400,700|Libre+Franklin:300,400,500,600' );
	// wp_enqueue_script( 'site_wide_js', get_template_directory_uri() . '/js/astrobhasha.min.js', array(),true );

}
add_action( 'wp_enqueue_scripts', 'astrobhasha_scripts' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }	
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}


function custom_youtube_shortcode() {
    static $already_run = false;
    if ( $already_run !== true ) {
	echo do_shortcode("[youtube_channel]");
	return; 
    }
    $already_run = true;
}
add_shortcode('youtube', 'custom_youtube_shortcode');





add_action( 'init', 'bookworm_blog_cpt' );

function bookworm_blog_cpt() {

register_post_type( 'book', array(
  'labels' => array(
    'name' => 'Books',
    'singular_name' => 'Book',
   ),
  'description' => 'Books which we will be discussing on this blog.',
  'public' => true,
  'menu_position' => 20,
  'supports' => array( 'title', 'editor', 'custom-fields' )
));
}