

 <h2>Recent Posts</h2>
<ul>
<?php
    $recent_posts = wp_get_recent_posts();
    foreach( $recent_posts as $recent ) {
        printf( '<p><a href="%1$s">%2$s</a></p>',
            esc_url( get_permalink( $recent['ID'] ) ),
            apply_filters( 'the_title', $recent['post_title'], $recent['ID'] )
        );
    }
?>
</ul>

 <?php  include_once('getcategory.php');?>
<h2>Recent Archieves</h2>
<?php wp_get_archives();?>