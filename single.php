<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package astrobhasha
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
		while ( have_posts() ) : the_post();?>



			<div class="blog-title">
								<?php the_title(sprintf('<h3 class="entry-title"><a href="%s" rel="bookmark">',  esc_url(get_permalink())), '</a></h3>'); ?>
						</div>
					
						<div class="blog-date">
							<span class="entry-date"><?php echo get_the_date(); ?></span>
						</div>
					
						<div class="blog-sample-images">
							<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail();
							} else { ?>
							<img src="<?php bloginfo('template_directory'); ?>/image/a.jpg" height="250px" width="376"/>
							<?php } ?>
						</div>

				<?php get_template_part( 'content',get_post_format()); ?>
						<div class="blog-desc">
							<?php the_content();?>
							
						</div>
			<?php // get_template_part( 'template-parts/content', get_post_type() );

			// the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			// if ( comments_open() || get_comments_number() ) :
			// 	comments_template();
			// endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
	<?php get_footer(); ?>

